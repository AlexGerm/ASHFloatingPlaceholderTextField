//
//  ASHFloatingPlaceholderTextField.swift
//  ASHFloatingPlaceholderTextField
//
//  Created by Alexander Shmakov on 15.07.16.
//  Copyright © 2016 Alexander Shmakov. All rights reserved.
//

import UIKit
import SnapKit

class ASHFloatingPlaceholderTextField: UITextField {
    
    @IBOutlet weak var prevTextField: UITextField?
    @IBOutlet weak var nextTextField: UITextField?
    
    private let placeholderLabel = UILabel()
    private let bottomBorder = UIView()
    private let editView = UIImageView()
    
    // Цвета
    
    @IBInspectable var validColor = UIColor.hx_colorWithHexString("4BBFB4")
    @IBInspectable var invalidColor = UIColor.hx_colorWithHexString("fc684f")
    
    // Настройки плейсхолдера
    
    private let placeholderAnimationDuration: Double = 0.3
    @IBInspectable var placeholderIsAnimated: Bool = true               // Уплывающий ли плейсхолдер
    @IBInspectable var placeholderDistanceFromText: CGFloat = 25.0      // Расстояние поднятного плейсхолдера от исходного состояния
    
    // Настройки текста
    
    @IBInspectable var textToEditViewDistance: CGFloat = 33.0           // Расстояние от текста до иконки редактирования
    
    // Настройки нижней границы
    
    @IBInspectable var borderDistance: CGFloat = 18.0                   // Расстояние от нижней границы до середины плейсхолдера
    @IBInspectable var borderHeight: CGFloat = 2.0                      // Высота нижней границы
    
    // Иконка редактирования
    
    @IBInspectable var rightImage = UIImage(named: "pencil-cyan-icon")!
    @IBInspectable var invalidRightImage = UIImage(named: "pencil-red-icon")!
    
    var valid: Bool = true {
        didSet {
            changeValidity()
        }
    }
    
    // MARK: - Иницализация
    
    override var text: String? {
        set {
            super.text = newValue
            if let newValue = newValue where !newValue.isEmpty {
                animatePlaceholderUp()
            }
        }
        get {
            return super.text
        }
    }
    
    override var placeholder: String? {
        set {
            super.placeholder = newValue
            
            if let newValue = newValue where !newValue.isEmpty {
                placeholderLabel.text = newValue
            }
        }
        get {
            return super.placeholder
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaceholderTextField.textFieldDidBeginEditing(_:)), name: UITextFieldTextDidBeginEditingNotification, object: self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PlaceholderTextField.textFieldDidEndEditing(_:)), name: UITextFieldTextDidEndEditingNotification, object: self)
    }
    
    func setup() {
        self.clipsToBounds = self.frame.height <= 0.0 ? true : false
        setupEditView()
        setupPlaceholder()
        setupBorder()
        setupSwitching()
    }
    
    func setupEditView() {
        
        self.addSubview(editView)
        editView.image = rightImage
        editView.contentMode = .Right
        editView.opaque = false
        editView.userInteractionEnabled = true
        
        editView.snp_makeConstraints { (make) in
            make.height.equalTo(self)
            make.width.equalTo(17.0 + textToEditViewDistance)
            make.bottom.equalTo(self)
            make.right.equalTo(self)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(PlaceholderTextField.editTextField))
        editView.addGestureRecognizer(tap)
    }
    
    func setupPlaceholder() {
        if placeholderIsAnimated {
            self.addSubview(placeholderLabel)
            placeholderLabel.text = placeholder ?? ""
            placeholder = ""
            placeholderLabel.font = Constants.UI.placeholderLabel.font!
            placeholderLabel.textColor = Constants.UI.placeholderLabel.color
            let placeholderBottomOffset: CGFloat = text != nil || text!.isEmpty ? 0.0 : placeholderDistanceFromText
            placeholderLabel.snp_makeConstraints { (make) in
                make.left.equalTo(self)
                make.right.equalTo(editView.snp_left)
                make.centerY.equalTo(self).offset(placeholderBottomOffset)
            }
        }
    }
    
    func setupBorder() {
        self.addSubview(bottomBorder)
        bottomBorder.backgroundColor = validColor
        bottomBorder.snp_makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.centerY.equalTo(self).offset(borderDistance)
            make.height.equalTo(borderHeight)
        }
    }
    
    func changeValidity() {
        bottomBorder.backgroundColor = self.valid ? validColor : invalidColor
        editView.image = self.valid ? rightImage : invalidRightImage
    }
    
    // MARK: - Анимация
    
    func animatePlaceholderUp() {
        if placeholderIsAnimated {
            
            placeholderLabel.snp_updateConstraints(closure: { (make) in
                make.centerY.equalTo(self).offset(-placeholderDistanceFromText)
            })
            
            UIView.animateWithDuration(placeholderAnimationDuration, animations: { [weak self]() in
                self?.layoutIfNeeded()
                })
            
        }
    }
    
    func animatePlaceholderDown() {
        if placeholderIsAnimated {
            
            placeholderLabel.snp_updateConstraints(closure: { (make) in
                make.centerY.equalTo(self).offset(0.0)
            })
            
            UIView.animateWithDuration(placeholderAnimationDuration, animations: { [weak self]() in
                self?.layoutIfNeeded()
                })
            
        }
    }
    
    // MARK: - Переключение между полями
    
    func setupSwitching() {
        let aRect = CGRect(x: 0, y: 0, width: CGRectGetWidth(UIScreen.mainScreen().bounds), height: 44)
        let inputAccessoryView = UIToolbar(frame: aRect)
        
        let prevButton = UIBarButtonItem(image: UIImage(named: "backward-cyan-icon"), style: .Plain, target: self, action: #selector(PlaceholderTextField.prev))
        let nextButton = UIBarButtonItem(image: UIImage(named: "forward-cyan-icon"), style: .Plain, target: self, action: #selector(PlaceholderTextField.next))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "TTL_DONE".localized, style: .Plain, target: self, action: #selector(PlaceholderTextField.done))
        prevButton.width = 30
        nextButton.width = 50
        prevButton.enabled = prevTextField != nil
        nextButton.enabled = nextTextField != nil
        inputAccessoryView.setItems([prevButton, nextButton, flexibleSpace, doneButton], animated: true)
        
        self.inputAccessoryView = inputAccessoryView
    }
    
    func prev() {
        prevTextField?.becomeFirstResponder()
    }
    
    func next() {
        nextTextField?.becomeFirstResponder()
    }
    
    func done() {
        self.resignFirstResponder()
    }
    
    // MARK: - Actions
    func editTextField() {
        self.becomeFirstResponder()
    }
    
    
    // MARK: - Observers
    func textFieldDidBeginEditing(sender: AnyObject) {
        if let text = self.text where text.isEmpty{
            animatePlaceholderUp()
        }
    }
    
    func textFieldDidEndEditing(sender: AnyObject) {
        if let text = self.text where text.isEmpty{
            animatePlaceholderDown()
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
}
